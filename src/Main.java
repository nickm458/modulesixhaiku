public class Main {
    public static void main(String[] args) {
        String [][] strings =
                {
                        { "An", "Old", "Silent", "Pond..." },
                        { "A", "frog", "jumps", "into", "the", "pond," },
                        { "splash!", "Silence", "again." }

                };

        System.out.println();
        for (String[] line : strings)
        {
            String printString = "";
            for (String word : line)
            {
                printString = printString.substring(0, printString.length()-2);
                System.out.println(printString);
            }
        }
    }
}
